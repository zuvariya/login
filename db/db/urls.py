from django.conf.urls import patterns, include, url
import os.path
STATIC_ROOT=os.path.join(os.path.dirname(__file__),'templates/static')
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from dataapp.views import *
urlpatterns = patterns('',
    # Examples:
    url(r'^$',fun1),
    url(r'^log/$',user),
    url(r'^ser/$',search),
    url(r'^view/$',view),
    url(r'^reg/$',reg1),
    url(r'^logout/$',logout),
    url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve',

     {'document_root': STATIC_ROOT}),


    # url(r'^db/', include('db.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
