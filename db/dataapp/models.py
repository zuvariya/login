from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class test(models.Model):
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=40)
    usertype=models.CharField(max_length=40)
    def __str__(self):
        return self.username
class register(models.Model):
    name = models.CharField(max_length=30)
    age = models.CharField(max_length=2)
    sex = models.CharField(max_length=50)
    address=models.CharField(max_length=50)
    phno=models.CharField(max_length=10)
    def __str__(self):
         return '%s %s %s %s %s' %(self.name,self.age,self.sex,self.address,self.phno)    
        #return self.name